/*
 * @Author: xiaosihan 
 * @Date: 2024-04-22 22:11:25 
 * @Last Modified by: xiaosihan
 * @Last Modified time: 2024-04-22 22:13:21
 */

import { observable, configure } from "mobx";

configure({ enforceActions: "never" });

const earthStore = observable({

    //自动旋转
    autoRotation: true,
    setAutoRotation(autoRotation: boolean) {
        this.autoRotation = autoRotation;
    },

    //选中的点
    // 德国汉堡 Hamburg, Germany
    // 西班牙马德里  Madrid, Spain
    // 美国德州 Texas, USA
    // 中国北京 Beijing, China
    active: "",
    setActive(active: string) {
        this.active = active;
    }

});
export default earthStore;