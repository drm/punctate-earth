import { Vector3 } from "three"

export type MarkParam = {
    position: Vector3,
    title: string,
    content: string
}