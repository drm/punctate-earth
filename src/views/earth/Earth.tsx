/*
 * @Author: xiaosihan 
 * @Date: 2022-07-11 07:51:56 
 * @Last Modified by: xiaosihan
 * @Last Modified time: 2024-05-29 16:11:36
 */

import { Watermark } from "antd";
import earthRender from "./earthRender";
import styles from './index.module.less';
import watermarkePNG from "./watermarke.png";

const Earth = () => {
    return (
        <Watermark
            className={styles.home}
            width={103}
            height={22}
            image={watermarkePNG}
        >
            <div ref={dom => earthRender.setContainer(dom)} className={styles.container}></div>
        </Watermark>
    );
}

export default Earth;