/*
 * @Author: xiaosihan 
 * @Date: 2024-04-22 22:13:40 
 * @Last Modified by: xiaosihan
 * @Last Modified time: 2024-04-23 00:05:10
 */

import { BackSide, DoubleSide, FrontSide, Group, MeshBasicMaterial, MeshStandardMaterial, MeshStandardMaterialParameters, ShaderChunk, ShaderMaterial, Vector3 } from "three";
import MeshBase from "three-base/MeshBase";
import earthGLB from "./earth.glb?url";
import earthStore from "./earthStore";
import { round } from "lodash";
import utils from "@utils";
import threeLoader from "three-base/threeLoader";

// 地球模型
export default class EatrhMesh extends Group {

    constructor() {
        super();
    }

    //地球模型
    earthMesh = (() => {
        const earthMesh = new MeshBase(earthGLB);
        earthMesh.addEventListener("loaded", async () => {
            earthMesh.traverseMesh(mesh => {
                (mesh.material as MeshStandardMaterial).emissive.set("#80eaff");
                (mesh.material as MeshStandardMaterial).emissiveMap = null;
                (mesh.material as MeshStandardMaterial).emissiveIntensity = 1;
                (mesh.material as MeshStandardMaterial).roughness = 0;
                (mesh.material as MeshStandardMaterial).metalness = 0;
                //three事件
                mesh.userData = {
                    enableEvent: true
                };
                mesh.addEventListener("click", e => {
                    console.log(((e as any).point as Vector3).toArray().map(v => round(v, 3)).join(", "));
                    earthStore.setActive("");
                });
            });
            await earthMesh.center();
            this.add(earthMesh);
        });
        return earthMesh;
    })();
}

