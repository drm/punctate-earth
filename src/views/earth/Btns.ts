/*
 * @Author: xiaosihan 
 * @Date: 2024-01-13 11:07:37 
 * @Last Modified by: xiaosihan
 * @Last Modified time: 2024-04-29 06:34:54
 */
import { generateUUID } from "three/src/math/MathUtils";
import spriteModalPNG from "./spriteModal.png";
import earthStore from "./earthStore";
import { MarkParam } from "./type";
import { autorun } from "mobx";
import left_arrowPNG from "./left_arrow.png";
import right_arrowPNG from "./right_arrow.png";

// 切换按钮
export default class Btns {

    constructor(marks: Array<MarkParam> = []) {
        this.marks = marks;
        marks.map(m => {
            this.creatBtn(m);
        });
    }

    marks: Array<MarkParam> = [];

    root = (() => {
        const root = document.createElement("div");
        Object.assign(root.style, {
            position: "absolute",
            zIndex: 20,
            bottom: "5%",
            left: "0px",
            width: "100%",
            padding: "20px",
            pointerEvents: "none",
            textAlign: "center"
        });
        return root;
    })();

    //上一个
    pre() {
        if (this.marks.length) {
            const { active } = earthStore;
            const index = Math.max(this.marks.findIndex(m => m.title === active), 0);
            const i = (this.marks.length + index - 1) % this.marks.length;
            earthStore.setActive(this.marks[i].title);
        }
    }

    // 下一个
    next() {
        if (this.marks.length) {
            const { active } = earthStore;
            const index = Math.max(this.marks.findIndex(m => m.title === active), -1);
            const i = (this.marks.length + index + 1) % this.marks.length;
            earthStore.setActive(this.marks[i].title);
        }
    }

    //左箭头
    leftArrow = (() => {
        const leftArrow = document.createElement("span");
        Object.assign(leftArrow.style, {
            display: "inline-block",
            width: "86px",
            height: "17px",
            backgroundImage: `url(${left_arrowPNG})`,
            verticalAlign: "middle",
            marginRight: "10px",
            pointerEvents: "all",
            cursor: "pointer",
        });
        leftArrow.onclick = () => this.pre();
        this.root.appendChild(leftArrow);
        return leftArrow;
    })();

    //右箭头
    rightArrow = (() => {
        const rightArrow = document.createElement("span");
        Object.assign(rightArrow.style, {
            display: "inline-block",
            width: "86px",
            height: "17px",
            backgroundImage: `url(${right_arrowPNG})`,
            verticalAlign: "middle",
            marginLeft: "10px",
            pointerEvents: "all",
            cursor: "pointer",
        });
        rightArrow.onclick = () => this.next();
        setTimeout(() => {
            this.root.appendChild(rightArrow);
        }, 100);
        return rightArrow;
    })();

    //创建按钮
    creatBtn(markParam: MarkParam) {
        const span = document.createElement("span");
        span.className = this.btnClassName;
        span.onclick = () => {
            earthStore.setActive(markParam.title);
        }
        autorun(() => {
            const { active } = earthStore;
            span.className = [this.btnClassName, active === markParam.title && "active"].join(" ");
        });
        this.root.appendChild(span);
    }

    btnClassName = `btn_${generateUUID()}`;

    //样式
    style = (() => {
        const style = document.createElement("style");
        style.innerText = `
        .${this.btnClassName}{
            display: inline-block;
            width: 10px;
            height: 10px;
            border: 2px solid #a9a9a9;
            border-radius: 50%;
            cursor: pointer;
            pointer-events: all;
            margin: 0px 4px;
            background-color: #ffffff;
            vertical-align: middle;
        }
        .${this.btnClassName}.active{
            border: 2px solid #1970dd;
        }
        `;
        this.root.appendChild(style);
        return style;
    })();

}

