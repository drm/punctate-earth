/*
 * @Author: xiaosihan 
 * @Date: 2024-01-13 11:07:37 
 * @Last Modified by: xiaosihan
 * @Last Modified time: 2024-01-13 11:55:43
 */
import { generateUUID } from "three/src/math/MathUtils";
import spriteModalPNG from "./spriteModal.png";
import earthStore from "./earthStore";
import MiSansExtraLightOTF from "./MiSans-ExtraLight.otf?url";

// 文本弹窗
export default class HtmlSprite {

    constructor() {

    }

    root = (() => {
        const root = document.createElement("div");
        Object.assign(root.style, {
            position: "absolute",
            opacity: 0.1,
            zIndex: 1,
            top: "0px",
            left: "0px",
            padding: "20px",
            boxSizing: "border-box",
            borderRadius: "20px",
            backgroundColor: "#ffffff",
            boxShadow: "0px 2px 10px #9ba7b6"
        });
        return root;
    })();

    // 标题
    title = (() => {
        const title = document.createElement("div");
        title.innerText = "标题";
        Object.assign(title.style, {
            color: "#111111",
            fontSize: "38px",
            textAlign: "left",
            marginBottom: "10px",
            fontFamily: "MiSansExtraLight"
        });
        this.root.appendChild(title);
        return title;
    })();

    listClassName = `list_${generateUUID()}`;

    // 列表
    list = (() => {
        const list = document.createElement("div");
        list.className = this.listClassName;
        Object.assign(list.style, {
            width: "320px",
            overflowY: "auto",
            textAlign: "left",
            color: "#111111",
            wordBreak: "break-word",
            fontSize: "18px",
            padding: "8px 0px",
            boxSizing: "border-box",
            fontFamily: "MiSansExtraLight"
        });
        this.root.appendChild(list);
        return list;
    })();

    //样式
    style = (() => {
        const style = document.createElement("style");
        style.innerText = `
        .${this.listClassName}{
            overflow-y: auto;
            &::-webkit-scrollbar {
                width: 4px;
                height: 4px;
                background-color: rgba(0, 0, 0, 0);
            }
            &::-webkit-scrollbar-track {
                box-shadow: inset 0 0 6px rgba(0, 0, 0, 0);
                border-radius: 10px;
                background-color: rgba(0, 0, 0, 0);
            }
            &::-webkit-scrollbar-thumb {
                border-radius: 10px;
                box-shadow: inset 0 0 6px rgba(0, 0, 0, .3);
                background-color: #1890ff;
            }
        }
        @font-face {
            font-family: MiSansExtraLight;
            src: url(${MiSansExtraLightOTF});
        }  
        `;
        this.root.appendChild(style);
        return style;
    })();

    // 设置标题
    setTitle(title: string) {
        this.title.innerText = title;
    }

    //设置内容
    setContent(content: string = "") {
        this.list.innerText = content;
    }

    //是否显示
    setShow(show: boolean) {
        Object.assign(this.root.style, {
            transition: show ? "opacity 1s ease-in-out" : "none",
            opacity: show ? 1 : 0,
            pointerEvents: show ? "all" : "none"

        });
    }
    //设置位置
    setPosition(x: number, y: number) {
        const { clientHeight, clientWidth } = this.root;
        this.root.style.left = `${x + 40}px`;
        this.root.style.top = `${y - (clientHeight / 2)}px`;
    }

}