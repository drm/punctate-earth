import { Vector3 } from "three";
import { MarkParam } from "./type";

// 美国
export const USA: MarkParam = {
    position: new Vector3(-0.285, 0.647, 0.693),
    title: "Texas, USA",
    content: "Situated in Texas, our office and testing facility are geared towards seamlessly rolling out customized products tailored to the unique demands of the US market."
}

// 德国
export const Hamburg: MarkParam = {
    position: new Vector3(0.351, 0.902, -0.219),
    title: "Hamburg, Germany",
    content: "Located in Hamburg, our company's headquarters serves as a dynamic hub housing state-of-the-art testing and customer service centers."
}

// 西班牙
export const Madrid: MarkParam = {
    position: new Vector3(0.683, 0.697, 0.17),
    title: "Madrid, Spain",
    content: "In Madrid, we've collaborated with SGS, a renowned testing, inspection, and certification company, to establish a cutting-edge testing facility aimed at enhancing product reliability and safety."
}

// 北京
export const Beijing: MarkParam = {
    position: new Vector3(-0.082, 0.717, -0.678),
    title: "Beijing, China",
    content: "Our R&D Center in Beijing is dedicated to leading the way in hardware, software, and innovative solutions."
}
