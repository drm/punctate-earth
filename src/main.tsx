/*
 * @Author: xiaosihan 
 * @Date: 2022-08-20 17:08:13 
 * @Last Modified by: xiaosihan
 * @Last Modified time: 2023-03-28 19:18:24
 */
import ReactDOM from 'react-dom/client';
import './index.css';
import Earth from '@views/earth/Earth';


const Root = window.Root || (window.Root = ReactDOM.createRoot(document.getElementById('root')!));

Root.render(<Earth />);