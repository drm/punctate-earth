import react from '@vitejs/plugin-react';
import { fileURLToPath, URL } from 'url';
import vitePluginImp from 'vite-plugin-imp';

// https://vitejs.dev/config/
export default ({ mode }) => {

    const isDev = (mode === "development");
    const isProd = (mode === "production");

    return ({
        base: "./",
        // 全局替换
        define: {
        },

        build: {
            assetsInlineLimit: 0,
            minify: 'terser',
            terserOptions: {
                compress: {
                    drop_console: false,
                    drop_debugger: true,
                }
            },
            chunkSizeWarningLimit: 1024 * 100
        },
        plugins: [
            react(),
            vitePluginImp({
                optimize: true,
                libList: [
                    {
                        libName: 'antd',
                        libDirectory: 'es',
                        style: (name) => {
                            if (name === 'watermark') {
                                return ''; // 或者返回一个占位符路径  
                            }
                            return `antd/es/${name}/style`;
                        }
                    }
                ]
            })
        ],
        resolve: {
            alias: {
                "@views": fileURLToPath(new URL('./src/views', import.meta.url)),
                "@utils": fileURLToPath(new URL('./src/utils/utils.tsx', import.meta.url)),
                "@components": fileURLToPath(new URL('./src/components', import.meta.url)),
            }
        },
        css: {
            preprocessorOptions: {
                less: {
                    javascriptEnabled: true,// 支持内联 javascript
                },
            }
        },
        server: {
            proxy: {
                '/api': {
                    target: 'http://jsonplaceholder.typicode.com',
                    changeOrigin: true,
                    rewrite: (path) => path.replace(/^\/api/, '')
                },
            }
        }
    })

}
